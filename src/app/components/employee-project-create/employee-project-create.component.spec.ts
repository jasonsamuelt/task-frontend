import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeProjectCreateComponent } from './employee-project-create.component';

describe('EmployeeProjectCreateComponent', () => {
  let component: EmployeeProjectCreateComponent;
  let fixture: ComponentFixture<EmployeeProjectCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeProjectCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeProjectCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
