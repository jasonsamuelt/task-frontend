import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-employee-project-create',
  templateUrl: './employee-project-create.component.html',
  styleUrls: ['./employee-project-create.component.css'],
})
export class EmployeeProjectCreateComponent implements OnInit {
  product = {
    name: '',
    description: '',
    available: false,
  };
  employees: any;
  projects: any;
  submitted = false;
  constructor(private productService: ProductService) {}

  ngOnInit(): void {
    this.readEmployees();
    this.readProjects();
  }

  readEmployees(): void {
    this.productService.getEmployees().subscribe(
      (employees) => {
        this.employees = employees;
        console.log(employees);
      },
      (error) => {
        console.log(error);
      }
    );
  }
  readProjects(): void {
    this.productService.getProjects().subscribe(
      (projects) => {
        this.projects = projects;
        console.log(projects);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  createProduct(): void {
    const data = {
      name: this.product.name,
      description: this.product.description,
    };

    this.productService.create(data).subscribe(
      (response) => {
        console.log(response);
        this.submitted = true;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  newProduct(): void {
    this.submitted = false;
    this.product = {
      name: '',
      description: '',
      available: false,
    };
  }
}
