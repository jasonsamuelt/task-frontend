import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css'],
})
export class ProductCreateComponent implements OnInit {
  product = {
    name: '',
    description: '',
    available: false,
  };
  employees: any;
  departments: any;
  submitted = false;

  constructor(private productService: ProductService) {}

  // ngOnInit(): void {}
  ngOnInit(): void {
    this.readEmployees();
    this.readDepartments();
  }

  readEmployees(): void {
    this.productService.getEmployees().subscribe(
      (employees) => {
        this.employees = employees;
        console.log(employees);
      },
      (error) => {
        console.log(error);
      }
    );
  }
  readDepartments(): void {
    this.productService.getDepartments().subscribe(
      (departments) => {
        this.departments = departments;
        console.log(departments);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  createProduct(): void {
    const data = {
      name: this.product.name,
      description: this.product.description,
    };

    this.productService.create(data).subscribe(
      (response) => {
        console.log(response);
        this.submitted = true;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  newProduct(): void {
    this.submitted = false;
    this.product = {
      name: '',
      description: '',
      available: false,
    };
  }
}
