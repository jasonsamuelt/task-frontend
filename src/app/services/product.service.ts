import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const baseURL = 'http://13.233.207.40:3000';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private httpClient: HttpClient) {}

  public products: any = [];

  readAll(): Observable<any> {
    return this.httpClient.get(`${baseURL}/userDepartments`);
  }

  getUserProjects(): Observable<any> {
    return this.httpClient.get(`${baseURL}/userprojects`);
  }

  getEmployees(): Observable<any> {
    return this.httpClient.get(`${baseURL}/employees`);
  }

  getDepartments(): Observable<any> {
    return this.httpClient.get(`${baseURL}/departments`);
  }

  getProjects(): Observable<any> {
    return this.httpClient.get(`${baseURL}/projects`);
  }

  read(id: any): Observable<any> {
    return this.httpClient.get(`${baseURL}/${id}`);
  }

  create(data: any): Observable<any> {
    return this.httpClient.post(baseURL, data);
  }

  update(id: any, data: any): Observable<any> {
    return this.httpClient.put(`${baseURL}/${id}`, data);
  }

  delete(id: any): Observable<any> {
    return this.httpClient.delete(`${baseURL}/${id}`);
  }

  deleteAll(): Observable<any> {
    return this.httpClient.delete(baseURL);
  }

  searchByName(name: any): Observable<any> {
    return this.httpClient.get(`${baseURL}?name=${name}`);
  }
}
