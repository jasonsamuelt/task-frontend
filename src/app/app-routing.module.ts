import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ProductCreateComponent } from './components/product-create/product-create.component';
import { EmployeeProjectCreateComponent } from './components/employee-project-create/employee-project-create.component';
const routes: Routes = [
  { path: '', redirectTo: 'products', pathMatch: 'full' },
  { path: 'employeeList', component: ProductListComponent },
  { path: 'products/:id', component: ProductDetailsComponent },
  { path: 'employeeDepartment', component: ProductCreateComponent },
  { path: 'employeeProject', component: EmployeeProjectCreateComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
